// Create an Express server
// It should have two API endpoints: greet and guess game for numbers between 0 and 10
// Remember to include the necessary 'requires'; the server should handle HTTP POST requests and parse JSON data

const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

app.post('/greet', (req, res) => {
  res.send(`Hello ${req.body.name}`);
});

app.post('/guess', (req, res) => {
  const guess = req.body.guess;
  const number = Math.floor(Math.random() * 11); 

//varify that the inputs are valid
if (typeof guess !== 'number' || guess < 0 || guess > 10) {
  return res.status(400).send('Invalid guess');
}

// add unit test
const test = require('tape');

test('guess game', (t) => {
  t.plan(2);

  const guess = 5;
  const number = 7;

  t.notEqual(guess, number, 'guess should not equal number');

  guess = number;

  t.equal(guess, number, 'guess should now equal number');
});

app.listen(3000, () => {
  console.log('Server listening on port 3000');
});